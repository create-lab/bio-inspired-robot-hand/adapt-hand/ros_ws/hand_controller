# Copyright 2020 Yutaka Kondo <yutaka.kondo@youtalk.jp>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import xacro

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch_ros.actions import Node


def generate_launch_description():

    hand_controller_node = Node(
        package="hand_controller",
        executable="hand_controller_node",
        name="hand_controller_node",
        output="screen",
    )
    
    robot_description = os.path.join(get_package_share_directory("hand_controller"), "urdf", "hand_controller.urdf.xacro")
    robot_description_config = xacro.process_file(robot_description)
    robot_state_publisher = Node(
            package="robot_state_publisher",
            executable="robot_state_publisher",
            name="robot_state_publisher",
            parameters=[
                {"robot_description": robot_description_config.toxml()}],
            output="screen",
        )

    joint_state_publisher_node = Node(
        package='joint_state_publisher',
        executable='joint_state_publisher',
        name='joint_state_publisher',
        parameters=[{
            'source_list': ['franka_state_controller/joint_states','hand_wrist/joint_states','dynamixel_joint_state_publisher/joint_states'],
        }]
    )

    controller_config = os.path.join(
        get_package_share_directory(
            "dynamixel_block_description"), "controllers", "controllers.yaml"
    )

    dynamixel_block_ros2_control_node_node = Node(
            package="controller_manager",
            executable="ros2_control_node",
            parameters=[
                {"robot_description": robot_description_config.toxml()}, controller_config],
            output="screen",
        )

    joint_publisher_config = os.path.join(
        get_package_share_directory(
            "hand_controller"), "config", "params.yaml"
        )

    dynamixel_block_joint_state_broadcaster_node =   Node(
            package="controller_manager",
            executable="spawner",
            arguments=["dynamixel_joint_state_publisher", "--controller-manager", "/controller_manager", "-p", joint_publisher_config],
            output="screen",
        )

    dynamixel_block_position_controller_node =    Node(
            package="controller_manager",
            executable="spawner",
            arguments=["dynamixel_position_controller", "-c", "/controller_manager"],
            output="screen",
        )

    rviz_config = os.path.join(get_package_share_directory("hand_controller"), "config", "display.rviz")
    rviz_node = Node(
            package="rviz2",
            executable="rviz2",
            name="rviz2",
            arguments=["-d", rviz_config],
            output="screen",
        )

    return LaunchDescription([
        dynamixel_block_ros2_control_node_node,  
        dynamixel_block_joint_state_broadcaster_node,  
        dynamixel_block_position_controller_node,  
        joint_state_publisher_node,
        robot_state_publisher,
        rviz_node,
        hand_controller_node
     ])
