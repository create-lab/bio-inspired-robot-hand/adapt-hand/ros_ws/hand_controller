from typing import List

import rclpy
from rclpy.node import Node

from sensor_msgs.msg import JointState
from std_msgs.msg import Float64MultiArray

from helper_functions.motor_regulator import MotorRegulator
from helper_functions.hand_regulator import HandRegulator

import numpy as np
from copy import deepcopy as cp

class HandControllerNode(Node):

    def __init__(self):
        super().__init__('hand_controller_node')

        ## Subscribers
        # --> Demand position for the hand only (13 dof in treated servo space)
        self.hand_servo_demand_subscriber = self.create_subscription(Float64MultiArray, '/hand/servo_demand', self.hand_servo_demand_callback, rclpy.qos.qos_profile_sensor_data)
        
        # --> Demand position for the wrist only (2 dof in treated servo space)
        self.wrist_servo_demand_subscriber = self.create_subscription(Float64MultiArray, '/wrist/servo_demand', self.wrist_servo_demand_callback, rclpy.qos.qos_profile_sensor_data)
        
        # --> Demand position for the hand only (20 dof in joint space of the hand)
        self.hand_joint_demand_subscriber = self.create_subscription(JointState, '/hand/joint_demand', self.hand_joint_demand_callback, rclpy.qos.qos_profile_sensor_data)
        
        # --> Demand position for the hand only (20 dof in joint space of the hand)
        self.wrist_pose_demand_subscriber = self.create_subscription(Float64MultiArray, '/wrist/pose_demand', self.wrist_pose_demand_callback, rclpy.qos.qos_profile_sensor_data)

        # --> Current position from the dynamixel (15 dof in raw servo space)
        self.dynamixel_state_subscriber = self.create_subscription(JointState, '/dynamixel_joint_state_publisher/joint_states', self.dynamixel_callback, 10)


        ## Publishers
        # --> The joint positions (MCP, PIP, ...) of the hand and wrist
        self.hand_wrist_joint_publisher = self.create_publisher(JointState, '/hand_wrist/joint_states', 10)
        
        # --> Position commands for the dynamixel motors
        self.dynamxiel_joint_publisher = self.create_publisher(Float64MultiArray, '/dynamixel_position_controller/commands', 10)
        
        # --> Publishing the demand servo positions
        self.servo_position_publisher = self.create_publisher(Float64MultiArray, '/hand_wrist/servo_positions', 10)

        self.mainloop = self.create_timer(0.001, self.mainloop_callback) 

        # Decay constant (0 = No decay, 1 = Full decay --> nothing changes)
        self.decay = 0.85

        # Class instances
        self.motor_regulator = MotorRegulator()
        self.hand_regulator = HandRegulator()

        # Current position of the servos + a combined hand/wrist demand position
        self.current_servo_position = None
        self.raw_demand_servo_position = None

    def mainloop_callback(self):
        if self.raw_demand_servo_position is None:
            return

        # Apply decay
        raw_demand = cp(self.raw_demand_servo_position)
        curr_pos = cp(self.current_servo_position)
        for i, motion_name in enumerate(raw_demand.keys()):    
            raw_demand[motion_name] = curr_pos[motion_name]*self.decay + raw_demand[motion_name]*(1-self.decay)
            
        # Check servo limits
        wrist_limited_demand, _ = self.hand_regulator.check_wrist_pitch_yaw_limit(raw_demand)
        servo_demand = self.motor_regulator.check_motor_limits(wrist_limited_demand)
        
        # Convert to raw motor commands
        motor_demand = self.motor_regulator.treated_to_raw_motor_pos(servo_demand)

        # Publish
        dynamixel_pos_demand = Float64MultiArray()
        dynamixel_pos_demand.data = motor_demand
        self.dynamxiel_joint_publisher.publish(dynamixel_pos_demand)

    def hand_joint_demand_callback(self, msg: JointState):
        if self.raw_demand_servo_position is None:
            return
        
        joint_names = list(msg.name)
        joint_demands = list(msg.position)
        joint_dict = dict(zip(joint_names, joint_demands))

        servo_pos = self.hand_regulator.hand_pos_to_servo_pos(joint_dict)

        names = self.motor_regulator.treated_motor_order
        for i in range(13):
            self.raw_demand_servo_position[names[i]] = servo_pos[i]


    def hand_servo_demand_callback(self, msg: Float64MultiArray):
        if self.raw_demand_servo_position is None:
            return
        
        names = self.motor_regulator.treated_motor_order
        for i in range(13):
            self.raw_demand_servo_position[names[i]] = msg.data[i]
            
    def wrist_pose_demand_callback(self, msg: Float64MultiArray):
        if self.raw_demand_servo_position is None:
            return
        
        left, right = self.hand_regulator.pitch_yaw_to_wrist_motor_pos(msg.data[0], msg.data[1])

        self.raw_demand_servo_position["Wrist_Left"] = left
        self.raw_demand_servo_position["Wrist_Right"] = right

                
    def wrist_servo_demand_callback(self, msg: Float64MultiArray):
        if self.raw_demand_servo_position is None:
            return
        
        self.raw_demand_servo_position["Wrist_Left"] = msg.data[0]
        self.raw_demand_servo_position["Wrist_Right"] = msg.data[1]


    def dynamixel_callback(self, motor_msg: JointState):

        if self.motor_regulator.is_communication_success(motor_msg):

            self.current_servo_position = self.motor_regulator.raw_motor_pos_to_treated(motor_msg)

            if self.raw_demand_servo_position is None:
                self.raw_demand_servo_position = cp(self.current_servo_position)

            hand_wrist_joint_positions = self.hand_regulator.servo_pos_to_hand_wrist_pos(self.current_servo_position)

            for i, name in enumerate(hand_wrist_joint_positions["name"]):
                if i < 20:
                    params = self.hand_regulator.hand_config["sim params"][name]
                    hand_wrist_joint_positions["angle"][i]  = hand_wrist_joint_positions["angle"][i] * params["dir"] + np.radians(params["offset"])

            # Publish hand joint information
            out_msg = JointState() 
            out_msg.header.stamp = self.get_clock().now().to_msg()
            out_msg.name = hand_wrist_joint_positions["name"]
            out_msg.position = hand_wrist_joint_positions["angle"]
            self.hand_wrist_joint_publisher.publish(out_msg)

            # Publish tendon joint information
            servo_position = Float64MultiArray()
            servo_position.data = list(self.current_servo_position.values())
            self.servo_position_publisher.publish(servo_position)
        else:
            print("\n\n!!! COMMUNICATION ERROR DETECTED !!!\n\n")
    

def main(args=None):
    rclpy.init(args=args)

    hand_controller_node = HandControllerNode()

    rclpy.spin(hand_controller_node)

    hand_controller_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()